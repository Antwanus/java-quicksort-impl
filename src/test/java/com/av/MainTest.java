package com.av;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MainTest {
   int[] unsorted;
   int[] sorted;

   @BeforeEach
   void setup() {
      unsorted = new int[]{5, 1, 4, 12, 2, 9};
      sorted = new int[]{1, 2, 4, 5, 9, 12};
   }

   @Test
   void testSimpleArray() {
      QuickSorter.quicksort(unsorted);

      assertArrayEquals(sorted, unsorted);

   }
}